package isen.contactApp.DataSource;

import isen.contactApp.daos.DataSourceFactory;
import isen.contactApp.daos.PersonDao;
import isen.contactApp.domain.Person;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class DataSourceFactoryTestCase {

    private PersonDao personDao;
    private Person person1;
    private Person person2;

    @Before
    public void initTestPerson() throws SQLException {
        personDao = new PersonDao();
        personDao.remove();
        Person person = new Person("M","D","M","3","m","@",LocalDate.of(1998,6,7));
        person1 = personDao.save(person);
        person2 = personDao.save(person);
    }

    @Test
    public void shouldAddPeople() throws SQLException {
        Person p = new Person("S","D","S","6","S","@",LocalDate.of(1998,6,7));
        p = personDao.save(p);
        //Then
        try(Connection con = DataSourceFactory.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Person WHERE idperson = "+p.getId());
            ResultSet rs = ps.executeQuery())
        {
            assertThat(rs.next()).isTrue();
            assertThat(rs.getString("FirstName")).isEqualTo(p.getFirstName());
            assertThat(rs.getString("NickName")).isEqualTo(p.getNickName());
            assertThat(rs.next()).isFalse();
        }
    }

    @Test
    public void shouldUpdatePeople() throws SQLException {
        person1.setFirstName("C");
        person1.setLastName("C");
        person1.setNickName("C");
        personDao.update(person1.getId(),person1);
        //Then
        try(Connection con = DataSourceFactory.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Person WHERE idperson ="+person1.getId());
            ResultSet rs = ps.executeQuery())
        {
            assertThat(rs.next()).isTrue();
            assertThat(rs.getString("LastName")).isEqualTo(person1.getLastName());
            assertThat(rs.getString("NickName")).isEqualTo(person1.getNickName());
            assertThat(rs.next()).isFalse();
        }
    }

    @Test
    public void shouldGetPeople(){
        personDao.save(person1);
        Person p = personDao.get(person1.getId());
        //Then
        assertThat(person1.getLastName()).isEqualTo(p.getLastName());
        assertThat(person1.getNickName()).isEqualTo(p.getNickName());
    }

    @Test
    public void shouldDeletePeople() throws SQLException {
        personDao.save(person2);
        personDao.delete(person2.getId());
        //Then
        try(Connection con = DataSourceFactory.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Person WHERE idperson="+person2.getId());
            ResultSet rs = ps.executeQuery())
        {
            assertThat(rs.next()).isFalse();
        }
    }

    @Test
    public void shouldGetList() throws SQLException {
        Person.setPersonList(personDao.getList());
        int nb = 0;
        //Then
        try(Connection con = DataSourceFactory.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Person");
            ResultSet rs = ps.executeQuery())
        {
            while (rs.next())
                nb++;
        }
        assertThat(nb).isEqualTo(Person.getPersonList().size());
    }

    @Test
    public void shouldRemove() throws SQLException {
        personDao.remove();
        try (Connection con = DataSourceFactory.getDataSource().getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM Person");
             ResultSet rs = ps.executeQuery()) {
            assertThat(rs.next()).isFalse();
        }
    }

}
