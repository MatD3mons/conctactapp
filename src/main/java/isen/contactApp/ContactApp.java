package isen.contactApp;

import isen.contactApp.daos.PersonDao;
import isen.contactApp.service.StageService;
import isen.contactApp.service.ViewService;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre 
 *
 */
public class ContactApp extends Application {

    public static PersonDao personDao;

    public ContactApp() {

    }
    
    
    /**
     * Starts the application by displaying the launch screen
     * 
     * @param primaryStage the primary Stage
     */
    @Override
    public void start(Stage primaryStage) {
        personDao = new PersonDao();
        StageService.initPrimaryStage(primaryStage);
        StageService.showView(ViewService.getView("Menu"));
    }
    
    /**
     * The main method
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) { launch(args); }


}
