package isen.contactApp.daos;

import isen.contactApp.domain.Person;
import isen.contactApp.exception.PersonException;
import isen.contactApp.utile.Dates;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class PersonDao implements Dao<Integer, Person>{

    private final String TABLE_NAME = "person";
    
    /**
     * Update the database
     * 
     * @param key the Person's key
     * @param person the Person
     * @exception PersonException
     */
    @Override
    public void update(Integer key, Person person) throws PersonException {
        String UPDATE_QUERY = "UPDATE " + TABLE_NAME + " SET " + " lastname = ?,  " + "firstname = ?, " +
                "nickname = ?, " + "phone_number = ?, " + "address = ?, " + "email_address = ?, " + "birth_date = ? " + "WHERE idperson = ? ";
        try(Connection con = DataSourceFactory.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(UPDATE_QUERY))
        {
            ps.setString(1,person.getLastName());
            ps.setString(2,person.getFirstName());
            ps.setString(3,person.getNickName());
            ps.setString(4,person.getPhoneNumber());
            ps.setString(5,person.getAdress());
            ps.setString(6,person.getEmail());
            ps.setDate(7, Dates.convertToDate(person.getBirthday()));
            ps.setInt(8,key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new PersonException(e, "Error while trying to update Person " + person.getFirstName());
        }
    }
    
    /**
     * Save a new Person to the database
     * 
     * @param person the Person
     * @return Person
     * @exception PersonException
     */
    @Override
    public Person save(Person person) throws PersonException {
        String INSERT_QUERY = "INSERT INTO " + TABLE_NAME + " (lastname,firstname,nickname,phone_number,address,email_address,birth_date) VALUES(?,?,?,?,?,?,?)";
        try (Connection con = DataSourceFactory.getDataSource().getConnection();
             PreparedStatement ps = con.prepareStatement(INSERT_QUERY,Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, person.getLastName());
            ps.setString(2, person.getFirstName());
            ps.setString(3, person.getNickName());
            ps.setString(4, person.getPhoneNumber());
            ps.setString(5, person.getAdress());
            ps.setString(6, person.getEmail());
            ps.setDate(7, Dates.convertToDate(person.getBirthday()));
            ps.executeUpdate();
            try( ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next())
                    person.setId(rs.getInt(1));
            }catch (SQLException e) {
                throw new PersonException(e, "Error while Reponse to save Person " + person.getFirstName());
            }
        } catch (SQLException e) {
            throw new PersonException(e, "Error while Request to save Person " + person.getFirstName());
        }
        return person;
    }
    
    /**
     * Delete a Person from the database
     * 
     * @param key the Person's key
     * @exception PersonException
     */
    @Override
    public void delete(Integer key)throws PersonException {
        String DELETE_QUERY = "DELETE FROM " + TABLE_NAME + " WHERE idperson=?";
        try(Connection con = DataSourceFactory.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(DELETE_QUERY))
        {
            ps.setInt(1,key);
            System.out.println(ps);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new PersonException(e,"Error while trying to delete Person ");
        }
    }
    
    /**
     * Get a Person from the table
     * 
     * @param key the Person's key
     * @return Person
     * @exception PersonException
     */
    @Override
    public Person get(Integer key) throws PersonException {
        Person person = null;
        String GET_QUERY = "SELECT * FROM " + TABLE_NAME + " WHERE idperson=?";
        try(Connection con = DataSourceFactory.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(GET_QUERY))
        {
            ps.setInt(1,key);
            try(ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    person = new Person();
                    person.setId(rs.getInt(1));
                    person.setLastName(rs.getString(2));
                    person.setFirstName(rs.getString(3));
                    person.setNickName(rs.getString(4));
                    person.setPhoneNumber(rs.getString(5));
                    person.setAdress(rs.getString(6));
                    person.setEmail(rs.getString(7));
                    person.setBirthday(Dates.convertToLocalDate(rs.getDate(8)));
                }
            } catch (SQLException e) {
                throw new PersonException(e,"Error while trying to get Person ");
            }
        } catch (SQLException e) {
            throw new PersonException(e,"Error while trying to get Person ");
        }
        return person;
    }
    
    /**
     * Get the list of all people from the database
     * 
     * @return List<Person>
     * @exception PersonException
     */
    @Override
    public List<Person> getList() throws PersonException {
        List<Person> listOfPeople = new ArrayList<>();
        String GETList_QUERY = "SELECT * FROM " + TABLE_NAME;
        try(Connection con = DataSourceFactory.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(GETList_QUERY))
        {
            try(ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Person person = new Person();
                    person.setId(rs.getInt(1));
                    person.setLastName(rs.getString(2));
                    person.setFirstName(rs.getString(3));
                    person.setNickName(rs.getString(4));
                    person.setPhoneNumber(rs.getString(5));
                    person.setAdress(rs.getString(6));
                    person.setEmail(rs.getString(7));
                    person.setBirthday(Dates.convertToLocalDate(rs.getDate(8)));
                    listOfPeople.add(person);
                }
            } catch (SQLException e) {
                throw new PersonException(e,"Error while trying to get Person ");
            }
        } catch (SQLException e) {
            throw new PersonException(e,"Error while trying to get Person ");
        }

        return listOfPeople;
    }
    
    /**
     * Remove every Person in the database
     * 
     * @exception PersonException 
     */
    @Override
    public void remove() throws PersonException {
        String REMOVE_QUERY = "DELETE FROM " + TABLE_NAME;
        try(Connection con = DataSourceFactory.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(REMOVE_QUERY))
        {
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
