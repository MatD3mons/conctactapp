package isen.contactApp.daos;

import isen.contactApp.domain.Message;
import isen.contactApp.exception.PersonException;

import java.util.List;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class MessageDao implements Dao<Integer, Message> {
	
	/**
	 * Update a Message in the database
	 * 
	 * @param key a key
	 * @param dto a Message
	 * @exception PersonException
	 */
    @Override
    public void update(Integer key, Message dto) throws PersonException {

    }
    
    /**
     * Save a Message in the database
     * 
     * @param dto a Message
     * @return Message
     * @exception PersonException
     */
    @Override
    public Message save(Message dto) throws PersonException {
        return null;
    }
    
    /**
     * Delete a Message from the database
     *
     * @param key a key
     * @exception PersonException
     */
    @Override
    public void delete(Integer key) throws PersonException {

    }
    
    /**
     * Get a Message from the database
     * 
     * @param key a key
     * @return Message
     * @exception PersonException
     */
    @Override
    public Message get(Integer key) throws PersonException {
        return null;
    }
    
    /**
     * Get a List of Messages from the database
     * 
     * @return List<Message>
     * @exception PersonException
     */
    @Override
    public List<Message> getList() throws PersonException {
        return null;
    }
    
    /**
     * Remove the Messages from the database
     * 
     * @exception PersonException
     */
    @Override
    public void remove() throws PersonException {

    }
}
