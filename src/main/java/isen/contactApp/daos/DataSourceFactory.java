package isen.contactApp.daos;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import isen.contactApp.domain.Person;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class DataSourceFactory {
        private static MysqlDataSource dataSource;
        
        /**
         * Get the Data Source
         * 
         * @return DataSource
         */
        public static DataSource getDataSource() {
                if (dataSource == null) {
                        dataSource = new MysqlDataSource();
                        dataSource.setServerName("localhost");
                        dataSource.setPort(3306);
                        dataSource.setDatabaseName("isen");
                        dataSource.setUser("root");
                        dataSource.setPassword("");
                        InitDatabase();
                }
                return dataSource;
        }
        
        /**
         * Initialize the database
         */
        public static void InitDatabase(){
                String scriptFilePath = "/src/main/resources/isen/contactApp/sql/database-creation.sql";
                Path root = Paths.get(Paths.get("").toAbsolutePath().toString());
                try (BufferedReader fileReader = Files.newBufferedReader(Paths.get(root + scriptFilePath))){
                        Statement stmt = dataSource.getConnection().createStatement();
                        String line, lines = "";
                        while ((line = fileReader.readLine()) != null) {
                                lines = lines + line;
                                if (lines.endsWith(";")) {
                                        stmt.executeUpdate(lines);
                                        lines = "";
                                }
                        }
                } catch (SQLException | IOException e) {
                        e.printStackTrace();
                }
                PersonDao personDao = new PersonDao();
                personDao.remove();
                Person p = new Person("Theo","Couzon","T.C","42","10","theo.couzon@isen.yncre.fr", LocalDate.of(1998,9,9));
                personDao.save(p);
                p = new Person("Matthieu","Desmarescaux","M.D","12","iSen","matthieu.mesmarescaux@isen.yncre.fr",LocalDate.of(1998,6,7));
                personDao.save(p);
                p = new Person("Romain","Dermu","R.D","69","DEll","romain.dermu@isen.yncre.fr",LocalDate.of(1998,6,7));
                personDao.save(p);
                p = new Person("Alexandre","Somon","A.S","1","ABS","alexandre.somon@isen.yncre.fr",LocalDate.of(1998,6,7));
                personDao.save(p);
        }

}
