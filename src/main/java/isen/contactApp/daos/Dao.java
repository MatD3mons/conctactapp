package isen.contactApp.daos;

import isen.contactApp.exception.PersonException;

import java.util.List;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 * @param <K>
 * @param <T>
 */
public interface Dao<K, T> {

    /**
     * Replace element who has a certain key by element give
     * 
     * @param key
     * @param dto
     * @throws PersonException
     */
    void update(K key, T dto) throws PersonException;

    /**
     * Add in the table the element and return element with key
     * 
     * @param dto
     * @return T
     * @throws PersonException
     */
    T save(T dto) throws PersonException;

    /**
     * remove element who has this key from the table
     * 
     * @param key
     * @throws PersonException
     */
    void delete(K key) throws PersonException;

    /**
     * Return element of the table who has this key
     * 
     * @param key
     * @return
     * @throws PersonException
     */
    T get(K key) throws PersonException;

    /**
     * Return all elements of the Table
     * 
     * @return List<T>
     * @throws PersonException
     */
    List<T> getList() throws PersonException;

    /**
     * Remove the table
     * 
     * @throws PersonException
     */
    void remove() throws PersonException;
}
