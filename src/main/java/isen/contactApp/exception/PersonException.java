package isen.contactApp.exception;

import java.sql.SQLException;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class PersonException extends RuntimeException {

    public PersonException(Exception e) {
        super(e);
    }

    public PersonException(SQLException e, String s) {
        super(s);
        System.out.println(e);
    }
}
