package isen.contactApp.service;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class StageService {

    private StageService() {
        mainLayout = ViewService.getView("Background");
    }

    private static class StageServiceHolder {
        private static final StageService INSTANCE = new StageService();
    }

    private Stage primaryStage;

    private BorderPane mainLayout;
    
    /**
     * Initialize the primary Stage of the application
     * 
     * @param primaryStage the primary Stage
     */
    public static void initPrimaryStage(Stage primaryStage) {
        primaryStage.setTitle("Contact App");
        primaryStage.setScene(new Scene(StageServiceHolder.INSTANCE.mainLayout));
        primaryStage.setHeight(533);
        primaryStage.setWidth(687);
        primaryStage.show();

        StageServiceHolder.INSTANCE.primaryStage = primaryStage;
    }
    
    /**
     * Show a View of the application
     * 
     * @param rootElement
     */
    public static void showView(Node rootElement) {
        StageServiceHolder.INSTANCE.mainLayout.setCenter(rootElement);
    }
    
    /**
     * Close the application
     */
    public static void closeStage() {
        StageServiceHolder.INSTANCE.primaryStage
                .fireEvent(new WindowEvent(StageServiceHolder.INSTANCE.primaryStage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

}
