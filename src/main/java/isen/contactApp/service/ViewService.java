package isen.contactApp.service;

import isen.contactApp.ContactApp;
import javafx.fxml.FXMLLoader;

import java.io.IOException;
import java.util.Objects;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class ViewService {
	
	/**
	 * Get a View of the application
	 * 
	 * @param id the id
	 * @return <T>
	 */
    public static <T> T getView(String id) {
        return Objects.requireNonNull(getLoader(id)).getRoot();
    }
    
    /**
     * Find the path to the View we want to show
     * 
     * @param id the name of the View
     * @return FXMLLoader
     */
    private static FXMLLoader getLoader(String id) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ContactApp.class.getResource("view/" + id + ".fxml"));
            loader.load();
            return loader;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}
