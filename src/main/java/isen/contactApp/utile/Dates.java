package isen.contactApp.utile;

import java.time.LocalDate;
import java.sql.Date;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class Dates {
	
	/**
	 * Convert a standard Date to a LocalDate
	 * 
	 * @param dateToConvert the date to convert
	 * @return LocalDate
	 */
    public static LocalDate convertToLocalDate(Date dateToConvert) {
        if(dateToConvert == null){return null;}
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }
    
    /**
     * Convert a LocalDate to a standard Date
     * 
     * @param dateToConvert the date to convert
     * @return Date
     */
    public static Date convertToDate(LocalDate dateToConvert) {
        if(dateToConvert == null){return null;}
        return java.sql.Date.valueOf(dateToConvert);
    }
}
