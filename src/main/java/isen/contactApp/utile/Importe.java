package isen.contactApp.utile;

import ezvcard.VCard;
import ezvcard.io.text.VCardReader;
import ezvcard.property.*;
import ezvcard.util.TelUri;
import isen.contactApp.daos.PersonDao;
import isen.contactApp.domain.Person;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */

public class Importe {
	
	/**
	 * Import a List of people from the vCard to the database
	 * 
	 * @throws IOException
	 */
    public static void Import() throws IOException {
        //remove bdd to do a correct import
        PersonDao personDao = new PersonDao();
        personDao.remove();
        
        //Var initialization
        Path path = Paths.get("src/main/resources/isen/contactApp/utile/");
        File directory = new File(String.valueOf(path));
        File[] listOfFiles = directory.listFiles();
        String firstname = "";
        String lastname = "";
        String nickname = "";
        String mail = "";
        String phonenumber = "";
        String number = "";
        String addresse = "";
        Date date = null;
        LocalDate localdate = null;
        
        //Check if the directory exist
        if (Files.exists(path)){
            //Check if the directory is empty
            if (directory.length() > 0) {
                //For each file in the directory we take every parameter (name, email etc...) to create a new Person and att it in the database
                for (File files : listOfFiles) {
                    if (files.isFile()) {
                        String filename = files.getName();
                        if (filename.contains(".vcf")) {
                            File file = new File("src/main/resources/isen/contactApp/utile/" + filename);
                            VCardReader reader = new VCardReader(file);
                            VCard vcard;

                            //Get all the data from the vcf file
                            while ((vcard = reader.readNext()) != null) {

                                StructuredName lastnames = vcard.getStructuredName();
                                if (lastnames.getFamily() != null)
                                    lastname = lastnames.getFamily();

                                FormattedName firstnames = vcard.getFormattedName();
                                if (firstnames.getValue() != null)
                                    firstname = firstnames.getValue();

                                Nickname nicknames = vcard.getNickname();
                                if (nicknames.getValues().size() >= 1)
                                    nickname = nicknames.getValues().get(0);

                                List<Email> email = vcard.getEmails();
                                for (Email mails : email) {
                                    mail = mails.getValue();
                                }

                                Birthday bday = vcard.getBirthday();
                                date = bday.getDate();
                                if (date != null)
                                    localdate = Dates.convertToLocalDate(new java.sql.Date(date.getTime()));

                                List<Telephone> tel = vcard.getTelephoneNumbers();
                                for (Telephone test2 : tel) {
                                    TelUri phone = test2.getUri();
                                    phonenumber = phone.toString();
                                    number = phonenumber.substring(4, phonenumber.length());
                                }

                                List<Address> address = vcard.getAddresses();
                                for (Address test : address) {
                                    addresse = test.getStreetAddress();
                                }
                            }

                            //Create a new Person and add it in the database
                            Person p = new Person(firstname, lastname, nickname, number, addresse, mail, localdate);
                            personDao.save(p);
                        }
                    }
                }
            } else {
                System.out.println("Can't import because there is no file in the folder.");
            }
        }else{
            //We create the directory
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
