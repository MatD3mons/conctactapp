package isen.contactApp.utile;
import isen.contactApp.ContactApp;
import isen.contactApp.domain.Person;

import java.io.*;
import java.time.LocalDate;
import java.util.List;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class Exporte {
	
	/**
	 * Export the list of people from the database to the vCard
	 * 
	 * @throws IOException
	 */
    public static void export() throws IOException {
        System.out.println(ContactApp.personDao.getList());
        List<Person> personList = ContactApp.personDao.getList();
        if (personList.size() > 0){
            for (Person person : personList) {
                System.out.println(person);
                String filename = person.getFirstName() + " " + person.getLastName() + ".vcf";
                File f = new File("src/main/resources/isen/contactApp/utile/" + filename);
                FileOutputStream fop = new FileOutputStream(f);

                if (f.exists()) {
                    String str = "BEGIN:VCARD\n" +
                            "VERSION:4.0\n" +
                            "UID:" + person.getId() + "\n" +
                            "N:" + person.getLastName() + "\n" +
                            "FN:" + person.getFirstName() + "\n" +
                            "NICKNAME:" + person.getNickName() + "\n" +
                            "TEL;TYPE=home,voice;VALUE=uri:tel:" + person.getPhoneNumber() + "\n" +
                            "ADR;TYPE=home:;;" + person.getAdress() + "\n" +
                            "EMAIL:" + person.getEmail() + "\n" +
                            "BDAY:" + person.getBirthday() + "\n" +
                            "END:VCARD";
                    fop.write(str.getBytes());
                    //Now read the content of the vCard after writing data into it
                    BufferedReader br = null;
                    String sCurrentLine;
                    if(person.getLastName().equals("") && person.getFirstName().equals("") && person.getNickName().equals("") && person.getPhoneNumber().equals("") && person.getAdress() == null && person.getEmail().equals("") && person.getBirthday() == null ){
                        System.out.println("Can't create the vcf file because of missing parameters");
                    }else{
                        br = new BufferedReader(new FileReader("src/main/resources/isen/contactApp/utile/" + filename));
                        while ((sCurrentLine = br.readLine()) != null) {
                            System.out.println(sCurrentLine);
                        }
                        //close the output stream and buffer reader
                        fop.flush();
                        fop.close();
                        System.out.println("The data has been written");
                    }
                } else
                    System.out.println("This file does not exist");
            }
        } else{
            System.out.println("Database empty can't export");
        }
    }
}
