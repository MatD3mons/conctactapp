package isen.contactApp.view;

import isen.contactApp.service.StageService;
import isen.contactApp.service.ViewService;
import isen.contactApp.utile.Exporte;
import isen.contactApp.utile.Importe;
import javafx.fxml.FXML;

import java.io.IOException;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class WhatToDoController {
	
	/**
	 * Action of the Contact Button : Get to the View TableView
	 */
	public void handleContactButton(){
		StageService.showView(ViewService.getView("TableView"));
	}
	/**
	 * Import to Bdd
	 */
	@FXML
	private  void  imports(){
		try {
			Importe.Import();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Export from Bdd
	 */
	@FXML
	private  void  exports(){
		try {
			Exporte.export();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
