package isen.contactApp.view;

import isen.contactApp.service.StageService;
import isen.contactApp.service.ViewService;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class MenuController {
	
	/**
	 * Action of the Launch! Button : Get to the View WhatToDo
	 */
    public void handleLaunchButton(){
    	StageService.showView(ViewService.getView("WhatToDo"));
    }

}
