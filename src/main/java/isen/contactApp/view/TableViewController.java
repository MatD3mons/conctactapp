package isen.contactApp.view;

import isen.contactApp.ContactApp;
import isen.contactApp.domain.Person;
import isen.contactApp.service.StageService;
import isen.contactApp.service.ViewService;
import isen.contactApp.utile.Exporte;
import isen.contactApp.utile.Importe;
import javafx.fxml.FXML;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import javax.imageio.IIOException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */

public class TableViewController {
    @FXML private TableColumn<Object, Object> lastNamecol;
    @FXML private TableColumn<Object, Object> firstNamecol;
    @FXML private TableColumn<Object, Object> nickNamecol;
    @FXML private TableColumn<Object, Object> numbercol;
    @FXML private TableColumn<Object, Object> adresscol;
    @FXML private TableColumn<Object, Object> emailcol;
    @FXML private TableColumn<Object, Object> birthdaycol;

    @FXML private TableView<Person> contact;
    private TableView.TableViewSelectionModel<Person> selectionModel;

    @FXML private HBox btnBox;
    @FXML private HBox searchBox;
    @FXML private TextField searchField;

    @FXML private TextField firstnameField;
    @FXML private TextField lastnameField;
    @FXML private TextField nicknameField;
    @FXML private TextField phoneNumberField;
    @FXML private TextField adressField;
    @FXML private TextField emailField;
    @FXML private TextField birthdayField;
    @FXML private GridPane editBox;
    
    /**
     * Initialize the TableView
     */
    @FXML
    private void initialize(){
        lastNamecol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        firstNamecol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        nickNamecol.setCellValueFactory(new PropertyValueFactory<>("nickName"));
        numbercol.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        adresscol.setCellValueFactory(new PropertyValueFactory<>("adress"));
        emailcol.setCellValueFactory(new PropertyValueFactory<>("email"));
        birthdaycol.setCellValueFactory(new PropertyValueFactory<>("birthday"));
        update();
    }
    
    /**
     * Get the AddPerson View to add a new Person
     */
    @FXML
    private void add(){
        StageService.showView(ViewService.getView("AddPerson"));
    }
    
    /**
     * Delete the contacts
     */
    @FXML
    private  void delete(){
        int i = selectionModel.getSelectedIndex();
        if(i>=0 || i<contact.getItems().size()){
            System.out.println(i);
            Person p = contact.getItems().get(i);
            contact.getItems().remove(p);
            ContactApp.personDao.delete(p.getId());
        }
    }
    
    @FXML
    private void searchView(){
        btnBox.setVisible(false);
        searchBox.setVisible(true);
    }

    @FXML
    private void searchBack(){
        btnBox.setVisible(true);
        searchBox.setVisible(false);
        searchField.setText("");
        update();
    }
    
    /**
     * Search one or multiple people having the characters inputed in the Search Field
     */
    @FXML
    private void search(){
        update();
        String s = searchField.getText();
        Iterator<Person> i = contact.getItems().iterator();
        while (i.hasNext()) {
            Person p = i.next();
            if (p.getFirstName().toLowerCase().contains(s.toLowerCase())) continue;
            if (p.getLastName().toLowerCase().contains(s.toLowerCase())) continue;
            if (p.getNickName().toLowerCase().contains(s.toLowerCase())) continue;
            if (p.getAdress().toLowerCase().contains(s.toLowerCase())) continue;
            if (p.getEmail().toLowerCase().contains(s.toLowerCase())) continue;
            if (p.getPhoneNumber().toLowerCase().contains(s.toLowerCase())) continue;
            if (p.getBirthday().toString().toLowerCase().contains(s.toLowerCase())) continue;
            i.remove();
        }
    }
    
    /**
     * Edit the List of Contacts
     */
    @FXML
    private void edit(){
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        int i = selectionModel.getSelectedIndex();
        if(i>=0 || i<contact.getItems().size()){
            System.out.println(i);
            Person p = contact.getItems().get(i);
            firstnameField.setText(p.getFirstName());
            lastnameField.setText(p.getLastName());
            nicknameField.setText(p.getNickName());
            phoneNumberField.setText(p.getPhoneNumber());
            adressField.setText(p.getAdress());
            emailField.setText(p.getEmail());
            birthdayField.setText(p.getBirthday().format(dtf));
            editBox.setVisible(true);
            searchBox.setVisible(false);
            btnBox.setVisible(false);
            contact.getItems().remove(p);
            ContactApp.personDao.delete(p.getId());
        }
    }
    
    /**
     * Execute when a Person is added
     */
    @FXML
    private void done(){
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        Person p = new Person(firstnameField.getText(),lastnameField.getText(),nicknameField.getText(),
                phoneNumberField.getText(),adressField.getText(),emailField.getText(), LocalDate.parse(birthdayField.getText(),dtf));
        ContactApp.personDao.save(p);
        editBox.setVisible(false);
        btnBox.setVisible(true);
        update();
    }
    
    /**
     * Come back to the menu
     */
    @FXML
    private void backToMenu(){
        StageService.showView(ViewService.getView("WhatToDo"));
    }
    
    /**
     * Update the List of Contacts
     */
    private void update() {
        contact.getItems().clear();
        selectionModel= contact.getSelectionModel();
        selectionModel.setSelectionMode(SelectionMode.SINGLE);
        contact.getItems().addAll(ContactApp.personDao.getList());
    }

}
