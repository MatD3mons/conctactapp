package isen.contactApp.view;

import isen.contactApp.ContactApp;
import isen.contactApp.domain.Person;
import isen.contactApp.service.StageService;
import isen.contactApp.service.ViewService;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class AddPersonController {

    @FXML private TextField firstnameField;
    @FXML private TextField lastnameField;
    @FXML private TextField nicknameField;
    @FXML private TextField phoneNumberField;
    @FXML private TextField adressField;
    @FXML private TextField emailField;
    @FXML private TextField birthdayField;
    
    /**
     * Add a new Person to the database
     */
    @FXML
    protected void add(){
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        Person p = new Person(firstnameField.getText(),lastnameField.getText(),nicknameField.getText(),
                phoneNumberField.getText(),adressField.getText(),emailField.getText(), LocalDate.parse(birthdayField.getText(),dtf));
        ContactApp.personDao.save(p);
        back();
    }
    /**
     * Returns to the View TableView
     */
    @FXML
    private void back(){
        StageService.showView(ViewService.getView("TableView"));
    }

}
