package isen.contactApp.view;

import isen.contactApp.service.StageService;
import isen.contactApp.service.ViewService;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class BackgroundController {
	
	/**
	 * Close the application
	 */
    public void closeApplication() {
        StageService.closeStage();
    }
    
    /**
     * Get to the Menu View
     */
    public void gotoHome() {
        StageService.showView(ViewService.getView("Menu"));
    }

}
