package isen.contactApp.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Théo, Romain, Matthieu, Alexandre
 *
 */
public class Person {

    private static List<Person> personList = new ArrayList<>();

    private int id;
    private String firstName;
    private String lastName;
    private String nickName;
    private String phoneNumber;
    private String adress;
    private String email;
    private LocalDate birthday;

    public Person(String firstName, String lastName, String nickName, String phoneNumber, String adress, String email, LocalDate birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.phoneNumber = phoneNumber;
        this.adress = adress;
        this.email = email;
        this.birthday = birthday;
    }

    public Person() {
    }
    
    /**
     * Get a List of people
     * 
     * @return List<Person>
     */
    public static List<Person> getPersonList() {
        return personList;
    }
    
    /**
     * Set a List of people
     * 
     * @param personList a List of people
     */
    public static void setPersonList(List<Person> personList) {
        Person.personList = personList;
    }
    
    /**
     * Get the id of a Person
     * 
     * @return int
     */
    public int getId() {
        return id;
    }
    
    /**
     * Set the id of a Person
     * 
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * Get the last name of a Person
     * 
     * @return String
     */
    public String getLastName() {
        return lastName;
    }
    
    /**
     * Set the last name of a Person
     * 
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    /**
     * Get the first name of a Person
     * 
     * @return String
     */
    public String getFirstName() {
        return firstName;
    }
    
    /**
     * Set the first name of a Person
     * 
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    /**
     * Get the nickname of a Person
     * 
     * @return String
     */
    public String getNickName() {
        return nickName;
    }
    
    /**
     * Set the nickname of a Person
     * 
     * @param nickName the nickname
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    
    /**
     * Get the phone number of a Person
     * 
     * @return String
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    /**
     * Set the phone number of a Person
     * 
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    /**
     * Get the address of a Person
     * 
     * @return String
     */
    public String getAdress() {
        return adress;
    }
    
    /**
     * Set the address of a Person
     * 
     * @param adress the address
     */
    public void setAdress(String adress) {
        this.adress = adress;
    }
    
    /**
     * Get the email of a Person
     * 
     * @return String
     */
    public String getEmail() {
        return email;
    }
    
    /**
     * Set the email of a Person
     * 
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     * Get the birthday of a Person
     * 
     * @return LocalDate
     */
    public LocalDate getBirthday() {
        return birthday;
    }
    
    /**
     * Set the birthday of a Person
     * 
     * @param birthday the birthday
     */
    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
}
